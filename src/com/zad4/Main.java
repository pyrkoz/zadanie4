package com.zad4;

import java.io.*;
import java.math.BigInteger;
import java.util.Random;

class Methods {

    void firstMethod() {
        String strA = drawNumber();
        String strB = drawNumber();
        BigInteger numA = new BigInteger(strA);
        BigInteger numB = new BigInteger(strB);

        System.out.println("a = " + strA);
        System.out.println("b = " + strB);

        sumBigIntegers(numA, numB);
        partAddition(strA, strB);
    }

    void secondMethod() {
        String str = Methods.readNumbers();
        String[] splittedStr = str.split("-");   //zastosowany wczesniej separator wykorzystujemy teraz do rozdzielenia naszych dwoch liczb
        String strA = splittedStr[0];
        String strB = splittedStr[1];
        BigInteger numA = new BigInteger(strA);
        BigInteger numB = new BigInteger(strB);

        System.out.println(str);
        System.out.println("a = " + strA);
        System.out.println("b = " + strB);

        sumBigIntegers(numA, numB);
        partAddition(strA, strB);
    }

    /*
    Liczby przechowywane w stringach zostaja zsumowane za pomoca metody "dodawania pisemnego".
    W tym celu liczby zostaja przekonwertowane do tablic typu char i są dodawane po jednej liczbie
    od prawej stronie, zgodnie z zasadami dodawania pisemnego.
     */
    void partAddition(String a, String b) {
        char[] tabA, tabB;
        boolean carry = false;
        String result = "";

        int aLen = a.length();
        int bLen = b.length();

        if (aLen != bLen) {
            int diff = Math.abs(aLen - bLen);
            if (aLen > bLen)
                b = String.join("", "0".repeat(diff)) + b;
            else
                a = String.join("", "0".repeat(diff)) + a;
        }

        tabA = a.toCharArray();
        tabB = b.toCharArray();

        for (int i = (aLen > bLen ? aLen - 1 : bLen - 1); i >= 0; i--) {
            int x = Character.getNumericValue(tabA[i]);
            int y = Character.getNumericValue(tabB[i]);
            int sum = x + y;

            sum += (carry ? 1 : 0);
            if (sum > 9) {
                carry = true;
                sum -= 10;
            } else {
                carry = false;
            }
            result = String.valueOf(sum) + result;
        }
        if (carry) {
            result = "1" + result;
        }
        System.out.println(" Wynik pisemnie = " + result);
    }

    //metoda sumujaca liczby typu BigInteger (dla sprawdzenia poprawnosci rozwiazania)
    void sumBigIntegers(BigInteger numA, BigInteger numB) {
        BigInteger bigNumberSum = numA.add(numB);
        String bigStrSum = String.valueOf(bigNumberSum);
        System.out.println("Suma BigInteger = " + bigStrSum);
    }

    static String drawNumber() {
        Random random = new Random();
        BigInteger a = new BigInteger("10");

        BigInteger rangeOfRandomNumbers = a.pow(random.nextInt(100000) + 999);   // zakres losowanych liczb = 10^999-10^100000
        BigInteger numResult = new BigInteger(rangeOfRandomNumbers.bitLength(), random);

        return numResult.toString();
    }

    static void writeNumbers() {
        try {
            FileWriter fw = new FileWriter("zad4.txt");
            fw.write(drawNumber());
            fw.write("-");      //separator, ktory rozdzieli dwie liczby
            fw.write(drawNumber());
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static String readNumbers() {
        String s = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader("zad4.txt"));
            s = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }
}

public class Main {

    public static void main(String[] args) {
        Methods.writeNumbers();
        Methods method = new Methods();

        if (args.length == 1) {
            method.firstMethod();
        } else if (args.length == 2) {
            method.secondMethod();
        } else {
            System.out.println("Niepoprawna liczba argumentow!");
        }
    }
}
